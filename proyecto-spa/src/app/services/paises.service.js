import { PaisesRepository } from "./paises.repository";

export class PaisesService {

    constructor() {
        this.repository = new PaisesRepository;
    }


    getPaises() {
        return this.repository.getAllCountries();
    }

    getCapitales(capital) {
        return this.repository.getCountriesByCapital(capital);
    }

    getRegion(region) {
        return this.repository.getCountriesByRegion(region);
    }

    getCodigo(codigo) {
        return this.repository.getCountriesByCodigo(codigo);
    }

    getPais(pais) {
        return this.repository.getCountry(pais);
    }

    getCallingCode(code){
        return this.repository.getCountryByCallingCode(code);
    }


}