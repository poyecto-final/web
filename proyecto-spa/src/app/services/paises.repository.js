import axios from "axios";
export class PaisesRepository {

    getAllCountries() {
        return axios.get('https://restcountries.eu/rest/v2/');
    }

    getCountriesByCapital(capital) {
        return axios.get(`https://restcountries.eu/rest/v2/capital/${capital}/`);
    }


    getCountriesByRegion(region) {
        return axios.get(`https://restcountries.eu/rest/v2/region/${region}`);
    }

    getCountriesByCodigo(codigo) {
        return axios.get(`https://restcountries.eu/rest/v2/regionalbloc/${codigo}`);
    }

    getCountry(pais) {
        return axios.get(`https://restcountries.eu/rest/v2/name/${pais}`);
    }

    getCountryByCallingCode(code){
        return axios.get(`https://restcountries.eu/rest/v2/callingcode/${code}`)
    }

}