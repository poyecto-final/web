import { PaisesRepository } from "./paises.repository";
import { PaisesService } from "./paises.service";
import { PAISES } from "../../../fixtures/paises";
import { CAPITAL } from "../../../fixtures/capital";
import {REGION} from "../../../fixtures/region";
import {CODIGO} from "../../../fixtures/codigo";
import {PAIS} from "../../../fixtures/pais";
jest.mock('./paises.repository');


describe('Paises service', () => {

    beforeEach(() => {
        PaisesRepository.mockClear();
    })

    it('Should get countries', async () => {
        PaisesRepository.mockImplementation(() => {
            return {
                getAllCountries: () => {
                    return PAISES
                }
            }
        })

        const service = new PaisesService();
        const paises = await service.getPaises();
        expect(paises.data.length).toEqual(250);
    })
})

describe('get capital service', () => {

    beforeEach(() => {
        PaisesRepository.mockClear();
    })


    it('it should get a capital', async () => {
        PaisesRepository.mockImplementation(() => {
            return {
                getCountriesByCapital: () => {
                    return CAPITAL
                }
            }

        })

        const service = new PaisesService();
        const capital = await service.getCapitales();
        expect(capital.data.length).toEqual(1);
    })
})


describe('get region service', ()=>{
    beforeEach(() => {
        PaisesRepository.mockClear();
    })

    it('it should get a region', async () =>{
        PaisesRepository.mockImplementation(()=>{
            return {
                getCountriesByRegion: () =>{
                    return REGION
                }
            }
        })

        const service = new PaisesService();
        const region = await service.getRegion();
        expect(region.data.length).not.toBe(0);
    })
})


describe('get code service', ()=>{
    beforeEach(() => {
        PaisesRepository.mockClear();
    })

    it('it should get countries by region', async () =>{
        PaisesRepository.mockImplementation(()=>{
            return {
                getCountriesByCodigo: () =>{
                    return CODIGO
                }
            }
        })

        const service = new PaisesService();
        const codigo = await service.getCodigo();
        expect(codigo.data.length).not.toBe(0);
    })
})


describe('get a single country', () =>{
    beforeEach(() => {
        PaisesRepository.mockClear();
    })

    it('it should get a single country', async () =>{
        PaisesRepository.mockImplementation(()=>{
            return {
                getCountry: () =>{
                    return PAIS
                }
            }
        })

        const service = new PaisesService();
        const pais = await service.getPais();
        expect(pais.data.length).not.toBe(0);
    })
})