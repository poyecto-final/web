import axios from "axios";
import {PAISES} from "../../../fixtures/paises";
import {PAIS} from "../../../fixtures/pais";
import {CAPITAL} from "../../../fixtures/capital";
import {REGION} from "../../../fixtures/region";
import {CODIGO} from "../../../fixtures/codigo";
import {PaisesRepository} from './paises.repository';
jest.mock('axios');


describe('Paises repository', ()=>{

    beforeEach(()=>{
        axios.mockClear();
    })
    
    it('Sould load all countries', async()=>{


        axios.get = jest.fn().mockResolvedValue(PAISES);

        const repository = new PaisesRepository();
        const result = await repository.getAllCountries();
        expect(result.data.length).toEqual(250);
    }) 
    
    
})


describe('Pais repository', ()=>{
    beforeEach(()=>{
        axios.mockClear();
    })

    it('Sould get a country', async ()=>{
        const pais = "canada";
        axios.get = jest.fn().mockResolvedValue(PAIS);

        const repositoryPais = new PaisesRepository();
        const result = await repositoryPais.getCountry('canada');
        expect(result).not.toBe(0);
    })


})


describe('Capital respository', ()=>{
    beforeEach(()=>{
        axios.mockClear();
    })

    it('Should get a capital', async ()=>{
        const capital = "madrid";
        axios.get = jest.fn().mockResolvedValue(CAPITAL);

        const respositoryCapital = new PaisesRepository();
        const result = await respositoryCapital.getCountriesByCapital(capital);
        expect(result.data.length).toEqual(1);
    })
})


describe('Region repository', ()=>{
    beforeEach(()=>{
        axios.mockClear();
    })

    it('Should get a region', async ()=>{
        const region = "europe";
        axios.get = jest.fn().mockResolvedValue(REGION);

        const repositoryRegion = new PaisesRepository();
        const result = await repositoryRegion.getCountriesByRegion(region);
        expect(result.data.length).not.toBe(0);

    })
})


describe('code repository', () =>{
    beforeEach(()=>{
        axios.mockClear();
    })


    it('Should get a countries by id', async () =>{
        const codigo = "eu";
        axios.get = jest.fn().mockResolvedValue(CODIGO);

        const repositoryCodigo = new PaisesRepository();
        const result = await repositoryCodigo.getCountriesByCodigo(codigo);
        expect(result.data.length).not.toBe(0);
    })
})


