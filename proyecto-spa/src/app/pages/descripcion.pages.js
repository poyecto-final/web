import { LitElement, html, css } from 'lit';
import { PaisesService } from '../services/paises.service';

export class DescripcionPages extends LitElement {
    constructor() {
        super();
        this._paisesService = new PaisesService();

    }
    static get properties() {
        return {
            location: { type: Object },
            pais: { type: Array }
        };
    }
    async connectedCallback() {
        super.connectedCallback();
        await this._paisesService.getCallingCode(this.location.params.id).then(response => {
            this.pais = response.data;
        }).catch(console.log);
    }

    static get styles() {
        return css`
        .fade-in-image {
        animation: fadeIn 5s;
        -webkit-animation: fadeIn 5s;
        -moz-animation: fadeIn 5s;
        -o-animation: fadeIn 5s;
        -ms-animation: fadeIn 5s;
        }

        @keyframes fadeIn {
            0% {
                opacity: 0;
            }

            100% {
                opacity: 1;
            }
        }

        @-moz-keyframes fadeIn {
            0% {
                opacity: 0;
            }

            100% {
                opacity: 1;
            }
        }

        @-webkit-keyframes fadeIn {
            0% {
                opacity: 0;
            }

            100% {
                opacity: 1;
            }
        }

        @-o-keyframes fadeIn {
            0% {
                opacity: 0;
            }

            100% {
                opacity: 1;
            }
        }

        @-ms-keyframes fadeIn {
            0% {
                opacity: 0;
            }

            100% {
                opacity: 1;
            }
        }



        /* --- Card3 ---- */

        .Card3{
        position: relative;
        display:inline-block;
        width: 40%;
        height: 550px;
        margin: 3em 1em;
        overflow: hidden;
        background: #000;
        box-shadow: rgba(6, 33, 63, 0.129412) 0px 1px 2px 0px;
        -moz-transition: 0.5s;-o-transition: 0.5s;-webkit-transition: 0.5s;transition: 0.5s;
        }

        .Card3 img{
        position: absolute;
        top:50%;
        left:50%;
        -webkit-transform: translate(-50%, -50%);-moz-transform: translate(-50%, -50%);-ms-transform: translate(-50%, -50%);-o-transform: translate(-50%, -50%);transform: translate(-50%,-50%);
        -moz-transition: 0.5s;-o-transition: 0.5s;-webkit-transition: 0.5s;transition: 0.5s;
        }
        .Card3:hover img{top:30%;opacity: 0.5;}

        .Card3 .overlay{
        position: absolute;
        bottom:calc(-65% + 50px);
        left: 0px;
        width: 100%;
        height: 80%;
        background: #FFF;
        -moz-transition: 0.5s;-o-transition: 0.5s;-webkit-transition: 0.5s;transition: 0.5s;
        }
        .Card3:hover .overlay{bottom:0px;}

        .Card3 .overlay .line{width: 100%;text-align: left;line-height: 50px;padding:0em 1em;}
        .Card3 .overlay .content{text-align: left;padding: 1em;}

        @media screen and (max-width: 700px){

        .Card3{width: 100%;height: auto;margin: 1em 0em;}
        .Card3:hover img{top:30%;opacity: 0.5;}

        .Card3 .overlay{
        position: relative;
        bottom: 0px;
        left: 0px;
        width: 100%;
        height: 200px;
        }
        .Card3 img{
        position: relative;
        display: block;
        width: 100%;
        height: auto;
        -webkit-transform: translate(-50%, 0%);-moz-transform: translate(-50%, 0%);-ms-transform: translate(-50%, 0%);-o-transform: translate(-50%, 0%);transform: translate(-50%,0%);

        }
        .Card3:hover .overlay{bottom:0px;}
        .Card3:hover img{top:50%;opacity: 1;}
        }
        .texto{
        text-align:center;
        }
                
`;
    }

    render() {
        return html`
        <div class="texto">
            <h2 class="fade-in-image">Descripcion de Pais</h2>
            <div class="Card3">
                ${this.pais?.map(pais => html`
                <img src="${pais.flag}" />
                <div class="overlay">
                    <div class="line">
                        <h2 class="title">${pais.name}</h2>
                        <p>Mouse over full description</p>
                    </div>
                    <div class="content">
                        <p><b>Capital</b>--${pais.capital}</p>
                        <p><b>Region</b>--${pais.region}</p>
                        <p><b>Sub-region</b>--${pais.subregion}</p>
                        <p><b>Poblacion</b>--${pais.population.toLocaleString('en-US')} habitantes</p>
                        <p><b>Area</b>--${pais.area.toLocaleString('en-US')} km2</p>
                        <p><b>Nombre nativo</b>--${pais.nativeName}</p>
                        <p><b>Codigo alpha</b>--${pais.alpha2Code}</p>
                    </div>
                </div>
                `)}
            </div>
        </div>
`
    }

}
customElements.define('descripcion-pages', DescripcionPages);