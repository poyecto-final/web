import { css, html, LitElement, render } from 'lit';
import { PaisesService } from '../services/paises.service';

export class CapitalesPage extends LitElement {

    constructor() {
        super();
        this._paisesService = new PaisesService();
    }

    connectedCallback() {
        super.connectedCallback();
    }

    static get properties() {
        return {
            message: { type: String },
            paises: { type: Array }
        }
    }
    static get styles() {
        return css`
        table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        margin-top:2em;
        }

        td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
        }

        tr:nth-child(even) {
        background-color: #dddddd;
        }

        img {
        border-radius: 50%;
        height: 25px;
        }
        .boton{
            cursor: pointer;
        }

        .fade-in-image {
        animation: fadeIn 5s;
        -webkit-animation: fadeIn 5s;
        -moz-animation: fadeIn 5s;
        -o-animation: fadeIn 5s;
        -ms-animation: fadeIn 5s;
}

@keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-moz-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-webkit-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-o-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-ms-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}
        `;
    }

    render() {
        return html`
        <h2>Escribe la capital deseada:</h2>
        <input type="text" placeholder="Escribe capital" id="name" data-cy="name" style="width: 150px; height: 25px">
        <vaadin-button theme="primary contrast small" type="button" @click="${this.onSave}" id="capitalButton" data-cy="capitalButton" class="boton">Submit</vaadin-button>
        <div style="overflow-x:auto;" class="fade-in-image">
            <table>
                <tr>
                    <th>Nombre</th>
                    <th>Capital</th>
                    <th>Subregion</th>
                    <th>Bandera</th>
                    <th>Poblacion</th>
                </tr>
                <tr>
                    ${this.paises?.map(capitales => html`<td>${capitales.name}</td>
                    <td>${capitales.capital}</td>
                    <td>${capitales.subregion}</td>
                    <td><img src="${capitales.flag}" alt="imagen" /></td>
                    <td>${capitales.population.toLocaleString('en-US')} -- Habitantes</td>`)}
        
                </tr>
            </table>
        </div>
`;
    }

    async onSave() {
        await this._paisesService.getCapitales(this.inputCapital.value.toLowerCase()).then(response => {
            this.paises = response.data;
        }).catch(console.log);
        this.inputCapital.value = '';
    }

    get inputCapital() {
        return this.shadowRoot.getElementById('name')
    }

}
customElements.define('capitales-page', CapitalesPage);