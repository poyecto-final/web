import { css, html, LitElement, render } from 'lit';
export class HomePage extends LitElement {
    constructor() {
        super();
    }
    static get properties() {
        return {}
    }
    static get styles() {
        return css`
    h1,h3{
    text-align:center;
    margin-top:3em;
    margin-bottom:2em;
    }


    .fade-in-image {
    animation: fadeIn 5s;
    -webkit-animation: fadeIn 5s;
    -moz-animation: fadeIn 5s;
    -o-animation: fadeIn 5s;
    -ms-animation: fadeIn 5s;

    }
    .cards {
    display: flex;
    flex-wrap: wrap;
    margin-left: -8px;
    margin-right: -8px;
    margin-top: 2em;
    justify-content: center;
    align-items: center;
    }
    .cards__item {
    flex-basis: 25%;
    padding-left: 8px;
    padding-right: 8px;
    margin-top:1em;
    }

@keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-moz-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-webkit-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-o-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-ms-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}
        
        `;
    }
    render() {
        return html`
        <h1>Bienvenido a mi SPA de paises</h1>
        <hr />
        <h3>Elaborada con Vainilla JS, Lit Element, Css, Cypress, Jest, Docker <br> donde puedes consultar los datos actuales de cada pais en el mundo desde la API Rest Countries.</h3>
        <carousel-component class="fade-in-image"></carousel-component>
        <div class="cards fade-in-image">
            <div class="cards__item">
                <fer-javi></fer-javi>
            </div>
            <div class="cards__item">
                <fer-javi name="Api de Rest countries"
                    img="https://images.unsplash.com/flagged/photo-1585324853527-1c567d53bb72?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=838&q=80"
                    url="https://restcountries.eu/"></fer-javi>
            </div>
        </div>
        `;

    }
}
customElements.define('home-page', HomePage);