import { css, html, LitElement } from 'lit';
import { PaisesService } from '../services/paises.service';
import { Router } from '@vaadin/router';

export class PaisesPage extends LitElement {

    constructor() {
        super();
        this._paisesService = new PaisesService();
    }
    async connectedCallback() {
        super.connectedCallback();
       await this._paisesService.getPaises().then(response => {
            this.paises = response.data;
        }).catch(console.log);
    }
    static get properties() {
        return {
            paises: { type: Array }
        }
    }
    static get styles() {
        return css`

        table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        margin-top:2em;
        }
        td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
        }
        tr:nth-child(even) {
        background-color: #dddddd;
        }
        img {
        border-radius: 50%;
        height: 25px;
        }
        .ex1 {
        overflow-y: scroll;
        height:700px;
        }

        .fade-in-image {
        animation: fadeIn 5s;
        -webkit-animation: fadeIn 5s;
        -moz-animation: fadeIn 5s;
        -o-animation: fadeIn 5s;
        -ms-animation: fadeIn 5s;
        }

        .boton{
            cursor: pointer;
        }
        h2{
            margin-top:4em;
            text-align:center;
        }
        h3{
            margin-top:3em;
            text-align:center;
        }


@keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-moz-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-webkit-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-o-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-ms-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}
`;
    }
    render() {
        return html`
        <h2>Listado paises</h2>
        <hr />
        <h3><b>Paises con descripcion completa por ID</b></h3>
        <div style="overflow-x:auto;" class="ex1 fade-in-image">
            <table>
                <tr>
                    <th>Nombre</th>
                    <th>Capital</th>
                    <th>Subregion</th>
                    <th>Bandera</th>
                    <th>Poblacion</th>
                    <th>Ver mas</th>
                </tr>
                ${this.paises?.map(pais => html`
                <tr>
                    <td>${pais.name}</td>
                    <td>${pais.capital}</td>
                    <td>${pais.subregion}</td>
                    <td><img src="${pais.flag}" alt="imagen" /></td>
                    <td>${pais.population.toLocaleString('en-US')} -- Habitantes</td>
                    <td><vaadin-button theme="primary contrast small"  @click="${() => this.navegar(pais)}" class="boton">Ver mas</vaadin-button></td>
                </tr>
                `)}
        
            </table>
        </div>
        `;

    }
    navegar(country) {
        Router.go(`/paises/${country.callingCodes}`);
    }
}
customElements.define('paises-page', PaisesPage);