import { css, html, LitElement, render } from 'lit';
import { PaisesService } from '../services/paises.service';

export class RegionPage extends LitElement {

    constructor() {
        super();
        this._regionService = new PaisesService();
    }
    connectedCallback() {
        super.connectedCallback();
    }
    static get properties() {
        return {
            message: { type: String },
            region: { type: Array }
        }
    }
    static get styles() {
        return css`
                table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        margin-top:2em;
        }

        td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
        }

        tr:nth-child(even) {
        background-color: #dddddd;
        }

        img {
        border-radius: 50%;
        height: 25px;
        }

        .ex1 {
        overflow-y: scroll;
        height:500px;
        }

        .fade-in-image {
        animation: fadeIn 5s;
        -webkit-animation: fadeIn 5s;
        -moz-animation: fadeIn 5s;
        -o-animation: fadeIn 5s;
        -ms-animation: fadeIn 5s;
        }

        .boton{
            cursor: pointer;
        }

@keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-moz-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-webkit-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-o-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-ms-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}
        `;
    }



    render() {
        return html`
        <h2>Elige la region que deseas:</h2>
        <label for="name"><b>Regiones:</b></label>
        <select name="name" id="name" style="width: 150px; height: 30px">
            <option>Africa</option>
            <option>Americas</option>
            <option>Asia</option>
            <option>Europe</option>
            <option>Oceania</option>
        </select>
        <vaadin-button theme="primary contrast small"  type="button" @click="${this.onSave}" class="boton">Submit</vaadin-button>
        <div style="overflow-x:auto;" class="ex1 fade-in-image">
            <table>
                <tr>
                    <th>Nombre</th>
                    <th>Capital</th>
                    <th>Subregion</th>
                    <th>Bandera</th>
                    <th>Poblacion</th>
                </tr>
                ${this.region?.map(regiones => html`
                <tr>
                    <td>${regiones.name}</td>
                    <td>${regiones.capital}</td>
                    <td>${regiones.subregion}</td>
                    <td><img src="${regiones.flag}" alt="imagen" /></td>
                    <td>${regiones.population.toLocaleString('en-US')} -- Habitantes</td>
                </tr>
                `)}
        
            </table>
        </div>
`;
    }
    async onSave() {
        await this._regionService.getRegion(this.inputRegion.value).then(response => {
            this.region = response.data;
        }).catch(console.log);
    }
    get inputRegion() {
        return this.shadowRoot.getElementById('name');
    }
}
customElements.define('region-page', RegionPage);