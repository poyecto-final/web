import { css, html, LitElement, render } from 'lit';
import { PaisesService } from '../services/paises.service';
export class PaisPage extends LitElement {

    constructor() {
        super();
        this._paisService = new PaisesService();
    }
    connectedCallback() {
        super.connectedCallback();
    }
    static get properties() {
        return {
            message: { type: String },
            pais: { type: Array }
        }
    }
    static get styles() {
        return css`
        table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
        margin-top:2em;
        }

        td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
        }

        tr:nth-child(even) {
        background-color: #dddddd;
        }

        img {
        border-radius: 50%;
        height: 25px;
        }

        .fade-in-image {
        animation: fadeIn 5s;
        -webkit-animation: fadeIn 5s;
        -moz-animation: fadeIn 5s;
        -o-animation: fadeIn 5s;
        -ms-animation: fadeIn 5s;
        }

        .boton{
            cursor: pointer
        }

@keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-moz-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-webkit-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-o-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}

@-ms-keyframes fadeIn {
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
}
        `;
    }
    render() {
        return html`
        <h2>Escribe el pais deseado:</h2>
        <input type="text" placeholder="Escribe Pais" id="namePais" data-cy="namePais" style="width: 150px; height: 25px">
        <vaadin-button theme="primary contrast small" type="button" @click="${this.onSave}" id="paisButton" data-cy="paisButton"class="boton">Submit</vaadin-button>
        <div style="overflow-x:auto;" class="fade-in-image">
            <table>
                <tr>
                    <th>Nombre</th>
                    <th>Capital</th>
                    <th>Subregion</th>
                    <th>Bandera</th>
                    <th>Poblacion</th>
                </tr>
                <tr>
                    ${this.pais?.map(pais => html`<td>${pais.name}</td>
                    <td>${pais.capital}</td>
                    <td>${pais.subregion}</td>
                    <td><img src="${pais.flag}" alt="imagen" /></td>
                    <td>${pais.population.toLocaleString('en-US')} -- Habitantes</td>`)}
        
                </tr>
            </table>
        </div>
        
        `;

    }

    async onSave() {
        await this._paisService.getPais(this.inputCapital.value.toLowerCase()).then(response => {
            this.pais = response.data;
        }).catch(error=>{
            console.log(error);
        });
        this.inputCapital.value = '';
    }
    get inputCapital() {
        return this.shadowRoot.getElementById('namePais');

    }



}
customElements.define('pais-page', PaisPage);