import { LitElement, html } from 'lit';

class NavbarComponent extends LitElement {

  constructor() {
    super();
  }

  render() {
    return html`

    <style>
      .topnav {
      background: #232526;
      background: -webkit-linear-gradient(to top, #414345, #232526);  
      background: linear-gradient(to top, #414345, #232526); 
      overflow: hidden;
      position: fixed;
      z-index: 99;
      width:100%;

      }

.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav h2{
  color: #f2f2f2;
  margin-left: 1em;
  letter-spacing: 4px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #04AA6D;
  color: white;
}


.topnav .icon {
  display: none;
}

@media screen and (max-width: 700px){
  .topnav{
    margin-top:-70px;
  }

}

@media screen and (max-width: 400px){
  .topnav{
    margin-top:-70px;
  }

}
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <div class="topnav" id="myTopnav">
      <h2>Fernando Ramirez</h2>
    </div>
    
    
    `;
  }



}
customElements.define("navbar-component", NavbarComponent);