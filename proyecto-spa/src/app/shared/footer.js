import { LitElement, html, css } from 'lit';

export class FooterComponent extends LitElement {




    static get properties(){
        return {
            fecha: {type: Number}
        }
    }

    constructor(){
        super();
        this.fecha = new Date();
    }


    static get styles() {
        return css`
        footer {
        background: #232526;
        background: -webkit-linear-gradient(to top, #414345, #232526);
        background: linear-gradient(to top, #414345, #232526);
        padding: 10px;
        text-align: center;
        color: white;
        position: fixed;
        bottom: 0;
        height: 5vh;
        width:100%;
        z-index:50;
        margin-top:2em;
        }

        
        
        
        `;



    }



    render() {
        return html`
        <footer>
            <p>Derechos reservados ©${this.fecha.getFullYear()}</p>
        </footer>
        
        
        `;
    }


}
customElements.define('footer-component', FooterComponent);