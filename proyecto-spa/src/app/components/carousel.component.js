import { LitElement, html, css } from 'lit';
import { PaisesService } from '../services/paises.service';

export class CarouselComponent extends LitElement {

    static get properties() {
        return {
            items: { type: Array },
            paises: { type: Array },
            looping: { type: Boolean },
            _offset: { type: Number },

        };
    }

    constructor() {
        super();
        this._paisesService = new PaisesService();
        this.looping = true;
        this._firstIndex = 0;
        this._offset = 0;
    }

    connectedCallback() {
        super.connectedCallback();
        this._paisesService.getPaises().then(response => {
            this.paises = response.data;
        }).catch(console.log);
        window.addEventListener('resize', this._initiateContainers);

    }

    disconnectedCallback() {
        super.disconnectedCallback();
        window.removeEventListener('resize', this._initiateContainers);
    }

    static get styles() {
        return css`
        :host {
            --item-margin: 10px;
            --item-offset: 0px;
            --item-width: 150px;
            display: flex;
            flex-direction: row;
            font: normal 14px/1.4 Helvetica, Arial, sans-serif;
        }

        img{
            height:70px;
            width:100%;
            border-radius:10px;
        }
    
        .btn-next,
        .btn-prev {
            background: none;
            border: 0;
            color: #ff6200;
            cursor: pointer;
            font-size: 36px;
            outline: none;
        }
    
        .hidden {
            visibility: hidden;
        }
    
        #contents {
            display: flex;
            flex: 1;
            overflow: hidden;
            position: relative;
        }
    
        #contents::after {
            background: linear-gradient(to right, #232526 0%,transparent 3%,transparent 97%,#232526 100%);
            content: '';
            position: absolute;
            height: 100%;
            width: 100%;
        }
    
        article {
            box-shadow: 0 0 0 1px rgba(63, 63, 68, 0.05), 0 1px 3px 0 rgba(63, 63, 68, 0.15);
            box-sizing: border-box;
            flex-shrink: 0;
            margin: var(--item-margin);
            padding: 10px;
            transform: translateX(calc(-1 * var(--item-offset)));
            transition: transform 300ms;
            width: var(--item-width);
            text-align:center;
        }
        :host([looping]) article {
            transition: none;
        }
        `;
    }

    render() {
        return html`
        <button class="btn-prev" @click=${() => this._move('left')}
            >
            < </button> <div id="contents">
                ${this.paises?.map(pais => html`
                <article>
                    <img src="${pais.flag}" alt="imagen" />
                    <p><b>${pais.name}</b></p>
                    <p>${pais.capital}</p>
                </article>
                `)}
                </div>
                <button class="btn-next" @click=${() => this._move('right')}
                    >
                    >
                </button>
        `;
    }

    _move(direction) {
        const container = this.shadowRoot.getElementById('contents');
        const styles = getComputedStyle(this);
        const itemMargin = parseFloat(styles.getPropertyValue('--item-margin'));
        const itemWidth = parseFloat(styles.getPropertyValue('--item-width'));
        const itemTotalWidth = itemWidth + 2 * itemMargin;

        if (this.looping) {
            const items = container.querySelectorAll('article');
            const lastIndex = items.length - 1;

            if (direction === 'left') {
                this._firstIndex = this._firstIndex === 0 ? lastIndex : this._firstIndex - 1;
            } else {
                this._firstIndex = this._firstIndex === lastIndex ? 0 : this._firstIndex + 1;
            }


            for (let i = this._firstIndex; i < items.length; i++) {
                items[i].style.transform = `translateX(-${itemTotalWidth * this._firstIndex}px)`;
            }


            for (let i = 0; i < this._firstIndex; i++) {
                items[i].style.transform = `translateX(${itemTotalWidth * (items.length - this._firstIndex)}px)`;
            }
        } else {
            const itemsTotalWidth = itemTotalWidth * this.items.length;
            const buffer = itemsTotalWidth - container.clientWidth;

            if (direction === 'left') {
                this._offset = this._offset - itemTotalWidth >= 0
                    ? this._offset - itemTotalWidth
                    : 0;
            } else {
                this._offset = this._offset + itemTotalWidth > buffer
                    ? buffer
                    : this._offset + itemTotalWidth;
            }
        }
        this.style.setProperty('--item-offset', `${this._offset}px`);
    }



}
customElements.define('carousel-component', CarouselComponent);