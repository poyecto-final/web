import './main.css';

import './app/pages/home.page';
import './app/pages/paises.page';
import './app/pages/capitales.page';
import './app/pages/region.page';
import './app/pages/codigo.page';
import './app/pages/pais.page';
import './app/pages/descripcion.pages';

import './router'

import './app/shared/navbar';
import './app/shared/footer';
import './app/components/carousel.component';

import 'ferjavi-2021/fer-javi';
import '@vaadin/vaadin-button';
