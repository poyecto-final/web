import { Router } from '@vaadin/router';

const outlet = document.getElementById('outlet');
const router = new Router(outlet);
router.setRoutes([{
    path:'/',
    children:[
        { path: '/home', component: 'home-page'},
        { path: '/paises',
            children:[
                {path: '/', component: 'paises-page'},
                {path:'/:id',component:'descripcion-pages'}
            ]
        },
        { path: '/pais', component: 'pais-page'},
        { path: '/capitales', component: 'capitales-page'},
        { path: '/regiones', component: 'region-page'},
        { path: '/codigo', component: 'codigo-page'},
        { path: '(.*)', redirect: '/home' },
    ]
}]);
